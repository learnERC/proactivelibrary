package org.eclipse.acceleo.policyenforcergenerator.services;

import java.lang.reflect.Constructor;
import java.net.InterfaceAddress;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import com.google.common.primitives.Primitives;

import android.*;

import java.lang.reflect.*;

public class Service {
	
	public static String readProps() {
		String ret = "";
		
		try {
		InputStream inputStream = new FileInputStream(new File("").getAbsolutePath() + File.separator + "config.properties"); //getClass().getResourceAsStream("build.properties");
	
		Properties prop = new Properties();
		prop.load(inputStream);
		ret = prop.getProperty("editautomatonPath");
		} catch (IOException e) {}
		
		return ret;
	}

	
	public static String FILENAME = "";
	
	/**
	 * Returns path and filename previously defined.
	 *
	 * @return FILENAME.
	 */
	public static String getFILENAME() {
		if (FILENAME == "") {
			System.out.println("LEGGO DA FILE PROP");
			FILENAME = readProps();
		}
		
		return FILENAME;
	}

	/**
	 * Set the filename given the directory, called in service.mtl.
	 *
	 * @param directory.
	 *
	 */
	public static void setFILENAME(String directory) {
		org.eclipse.acceleo.policyenforcergenerator.services.Service.FILENAME = directory;
	}

	/**
	 * Takes as input the API, manipulates the string with method innerClassCheck,
	 * obtain the list of the class methods, for each method substitutes blankSpaces
	 * with char '%'
	 *
	 * @param api
	 * @return
	 * @throws ClassNotFundException
	 */
	public List<String> getPublicAndInheritedMethods(String api) throws ClassNotFoundException {
		if (api.contentEquals("empty")) {
			return Arrays.asList(api);
		}

		ArrayList<String> list = new ArrayList<>();
		Class<?> cls;

		String checkInner = innerClassCheck(api);
		//System.out.println("api: " + api + " check inner "+ checkInner);
		if (checkInner.equals("")) {
			cls = Class.forName(api);
		} else {
			cls = Class.forName(checkInner);
		}

		Method[] methods = cls.getDeclaredMethods();
		for (Method method : methods) {
			if (Modifier.isPublic(method.getModifiers())) {
				ArrayList<String> paramNames = new ArrayList<>();
				Class<?> declaredClass = method.getDeclaringClass();
				Class[] parameters = method.getParameterTypes();
				for (Class param : parameters) {
					paramNames.add(param.getName());
				}

				list.add(method.toGenericString() + "%" + method.getReturnType().toString() + "%" + declaredClass + "%" + method.getName() +"(" + paramNames + ")");
			}
		}
		return list;
	}

	/**
	 * Takes as input the method, with the args, and the API written in the
	 * Automata, manipulates the args and reconstructs with the syntax used in
	 * android.jar, returns the method with syntax as "returnType%method([args])
	 *
	 * @param methodString
	 * @param api
	 * @return
	 * @throws Exception
	 */
	public String getVarList(String methodString, String api) throws Exception {
		this.checkExistence(api, methodString);

		String methodName = "";
		String args = "";
		for (int i = 0; i < methodString.length(); i++) {
			if (methodString.charAt(i) == '(') {
				methodName = methodString.substring(0, i);
				args = methodString.substring(i + 1, methodString.length() - 1);
			}
		}

		args = args.replaceAll(" ", "");

		String[] parts = args.split(",");
		Class<?>[] classList = new Class[parts.length];
		boolean flagArgs = false;
		if (!args.equals("")) {
			flagArgs = true;

			for (int i = 0; i < parts.length; i++) {
				parts[i] = parts[i].replace(" ", "");
				if (parts[i].equals("int") || parts[i].equals("long") || parts[i].equals("byte")
						|| parts[i].equals("float") || parts[i].equals("double") || parts[i].equals("boolean")
						|| parts[i].equals("char") || parts[i].equals("short")) {
					classList[i] = parseType(parts[i]);
				} else {
					String strNoPar = parts[i].replaceAll("\\[", "").replaceAll("\\]", "");

					if (strNoPar.equals(parts[i])) {
						Class<?> cl = Class.forName(parts[i]);
						classList[i] = cl;
					} else {
						String o = "[L" + strNoPar + ";";
						Class<?> cl = Class.forName(o);
						classList[i] = cl;
					}

				}
			}
		}

		Class<?> cls;
		String checkInner = innerClassCheck(api);

		if (checkInner.equals("")) {
			cls = Class.forName(api);
		} else {
			cls = Class.forName(checkInner);
		}

		Method method;
		if (!flagArgs) {
			method = cls.getMethod(methodName);
		} else {
			method = cls.getMethod(methodName, classList);
		}

		ArrayList<String> paramNames = new ArrayList<>();
		Class<?>[] parameters = method.getParameterTypes();
		for (Class param : parameters) {
			paramNames.add(param.getName());
		}

		return method.getReturnType().toString().replaceAll("class ", "") + "%" + method.getName() + "(" + paramNames + ")";
	}

	/**
	 * Takes as input the API exactly as written in the Automata and manipulating
	 * the string returns the API as with the syntax defined in the android.jar or
	 * the empty string if the syntaxes are the same, is called by methods:
	 * getPublicAndInheritedMethods, getVarlist and checkExistence
	 *
	 * @param api
	 * @return
	 */
	public String innerClassCheck(String api) {
		// System.out.println("API "+api);
		String out = "";

		String[] parts = api.split("\\.");
		boolean classFound = false;
		boolean flag = false;
		for (String part : parts) {
			if (Character.isUpperCase(part.charAt(0))) {
				if (classFound) {
					flag = true;
				} else {
					classFound = true;
				}
			}
		}
		int len = parts.length;
		if (flag) {
			for (int i = 0; i < len; i++) {
				if (i != len - 2) {
					out = out + parts[i] + ".";
				} else {
					out = out + parts[i] + "$";
				}
			}
		}
		if (out.length() > 1) {
			out = out.substring(0, out.length() - 1);
		}
		return out;
	}

	/**
	 * verifies if the file defined in variable FILENAME (path of the Automata)
	 * exists
	 *
	 * @param s1
	 * @return
	 * @throws FileNotFoundException
	 */
	public String fileContained(String s1) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(getFILENAME()));
		List<String> list = new ArrayList<>();

		while (scanner.hasNextLine()) {
			list.add(scanner.nextLine());
		}

		for (String str : list) {
			if (str.trim().contains(s1)) {
				return "contained";
			}
		}

		return "notcontained";
	}

	/**
	 * Takes as input one by one all the methods of the API with the syntax of the
	 * android.jar does the parsing of the file .editautomaton and returns true if
	 * the method taken as input is an Automata action
	 *
	 * @param method
	 * @return
	 */
	public static boolean isAutomataAction(String method) {

		BufferedReader br = null;
		FileReader fr = null;
		try {
			fr = new FileReader(getFILENAME());
			br = new BufferedReader(fr);
			String line;
			method = method.replace("[", "").replace("]", "");
			String strPattern = method.replace('$', '.');

			while ((line = br.readLine()) != null) {
				if (line.contains(strPattern)) {
					br.close();
					return true;
				} else if (line.contains(strPattern.replace("(", "").replace(")", ""))) {
					br.close();
					return true;
				}
			}
			
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * returns the API corresponding to the resource, defined in the Automata
	 *
	 * @return
	 * @throws Throwable
	 */
	public String getApi() throws Throwable {
		return getHandledObject();
	}

	/**
	 * Takes as input one by one all the methods written in the syntax of the
	 * android.jar and returns the parameters with the syntax accepted by
	 * "findAndHookMethod"
	 *
	 * @param method
	 * @return
	 */
	public static String extractParameters(String method) {
		System.out.println("ExtractParameters(" + method + ")");

		int startIndex = 0;
		boolean flag = false;
		for (int index = 0; index < method.length(); index++) {
			if (flag == false && method.charAt(index) == '[') {
				flag = true;
				startIndex = index + 1;
			} else if (method.charAt(index) == ']') {
				String paramStr = method.substring(startIndex, index);
				String[] parameters = paramStr.split(",");
				if (paramStr.equals("")) {
					return paramStr;
				} else {
					paramStr = "";
					for (String parameter : parameters) {
						// System.out.println("paramtero" + parameter);
						String parameterMod = parameter.replace('$', '.').replaceAll(" ", "");
						if (parameterMod.equals("[B")) {
							parameterMod = "byte[]";
						} else if (parameterMod.equals("[Z")) {
							parameterMod = "boolean[]";
						} else if (parameterMod.equals("[C")) {
							parameterMod = "char[]";
						} else if (parameterMod.equals("[S")) {
							parameterMod = "short[]";
						} else if (parameterMod.equals("[I")) {
							parameterMod = "int[]";
						} else if (parameterMod.equals("[J")) {
							parameterMod = "long[]";
						} else if (parameterMod.equals("[F")) {
							parameterMod = "float[]";
						} else if (parameterMod.equals("[D")) {
							parameterMod = "double[]";
						} else if (parameterMod.contains("[Ljava")) {
							String[] parts = parameterMod.split("\\.");
							int len = parts.length - 1;
							parameterMod = parts[len].replace(";", "") + "[]";
						}
						paramStr = paramStr + parameterMod + ".class, ";
					}
					paramStr = getProperParameters(paramStr);
					return paramStr;
				}

			}
		}
		return null;
	}

	/**
	 * returns the name of the class corresponding to the API in the android.jar an
	 * corrects the syntax
	 *
	 * @param api
	 * @return
	 */
	public String extractClass(String api) {

		String flag = innerClassCheck(api);

		String[] parts = api.split("\\.");
		boolean found = false;
		if (flag.equals("")) {
			for (String part : parts) {
				if (Character.isUpperCase(part.charAt(0))) {
					part = part.replace("()", "");

					return part;
				}
			}
		} else {
			String out = "";
			for (String part : parts) {
				if (Character.isUpperCase(part.charAt(0))) {
					if (!found) {
						found = true;
						out = out + part + ".";
					} else {
						out = out + part;
					}
					part = part.replace("()", "");
				}
			}

			return out;
		}

		return null;
	}

	/**
	 * takes in input the method in the syntax of the android.jar and returns the
	 * name without parameters
	 *
	 * @param method
	 * @return
	 */
	public static String extractMethodName(String method) {

		for (int index = 0; index < method.length(); index++) {
			if (method.charAt(index) == '(') {

				return (method.substring(0, index));
			}
		}
		return null;
	}

	/**
	 * Takes in input the parameters corrected according to the syntax accepted in
	 * the method findAndHookMethod and returns in a form that can be used in the
	 * hook method
	 *
	 * @param paramString
	 * @return
	 */
	public static String setParameters(String paramString) {

		String output = "";
		if (paramString.equals("")) {
			return "";
		} else {
			paramString = paramString.substring(0, paramString.length() - 1);
			String[] parameters = paramString.split(",");
			for (int i = 0; i < parameters.length; i++) {
				String[] parts = parameters[i].split("\\.");
				String cls = "";
				boolean foundFirst = false;
				for (String part : parts) {
					part = part.replace(" ", "");
					if (Character.isUpperCase(part.charAt(0))) {
						if (!foundFirst) {
							cls = part;
							foundFirst = true;
						} else {
							cls = cls + "." + part;
						}
					} else if (part.contains("int") | part.contains("boolean") | part.contains("char")
							| part.contains("byte") | part.contains("short") | part.contains("double")
							| part.contains("long") | part.contains("float")) {
						cls = part;
					}
				}
				output = output + "(" + cls + ")" + "param.args[" + i + "]" + ", ";
			}
		}

		return output.substring(0, output.length() - 2);
	}

	/**
	 * Takes as input the type of the parameters and returns the corresponding class
	 *
	 * @param className
	 * @return
	 */
	public static Class<?> parseType(final String className) {

		switch (className) {
		case "boolean":
			return boolean.class;
		case "byte":
			return byte.class;
		case "short":
			return short.class;
		case "int":
			return int.class;
		case "long":
			return long.class;
		case "float":
			return float.class;
		case "double":
			return double.class;
		case "char":
			return char.class;
		case "void":
			return void.class;
		default:
			String fqn = className.contains(".") ? className : "java.lang.".concat(className);
			try {
				return Class.forName(fqn);
			} catch (ClassNotFoundException ex) {
				throw new IllegalArgumentException("Class not found: " + fqn);
			}
		}
	}

	/**
	 * using readFile reads the file .editautomaton and returns the LifeCycleObject
	 * specified
	 *
	 * @return
	 * @throws Exception
	 */
	public static String getLifeCycleObject() throws Exception {
		String text = readFile(getFILENAME());
		Pattern p = Pattern.compile(".*api=\"(.+)\".*");
		Matcher m = p.matcher(text);
		while (m.find()) {
			String b = m.group(1);
			return b;
		}
		return null;
	}

	/**
	 * using readFile reads the file .editautomaton and returns the handledObject
	 * specified
	 *
	 * @return
	 * @throws Exception
	 */
	public static String getHandledObject() throws Exception {
		String text = readFile(getFILENAME());
		Pattern p = Pattern.compile(".*handledObject=\"([^\"]+)\"");
		Matcher m = p.matcher(text);
		while (m.find()) {
			String b = m.group(1);
			return b;
		}
		return null;
	}

	/**
	 * Reads the file .editautomaton saving the content in a string
	 *
	 * @param pathname
	 * @return
	 * @throws IOException
	 */

	private static String readFile(String pathname) throws IOException {

		File file = new File(pathname);
		StringBuilder fileContents = new StringBuilder((int) file.length());

		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNextLine()) {
				fileContents.append(scanner.nextLine() + System.lineSeparator());
			}
			return fileContents.toString();
		}
	}

	/**
	 * Takes as input the parameters of each method and corrects them in the form
	 * "[L"<class>";.class"
	 *
	 * @param correggere
	 * @return
	 */
	public static String getProperParameters(String correggere) {
		// System.out.println("correggere "+correggere);
		if (correggere.indexOf("[L") == -1) {
			return correggere;
		} else {
			String ret = "";
			String[] stringhe = correggere.split("\\,");
			int k;
			String tmp;
			boolean isempty = false;
			for (int i = 0; i < stringhe.length; ++i) {
				tmp = stringhe[i];
				if (tmp.equals(" "))
					isempty = true;
				if (tmp.indexOf("[L") != -1) {
					k = tmp.indexOf("android");
					tmp = tmp.substring(k);
					k = tmp.indexOf(";.class");
					tmp = tmp.substring(0, k);
					tmp = tmp + "[]" + ".class";
				}
				if (!isempty)
					ret = ret + tmp + ",";
			}

			return ret;
		}
	}

	public static String doubleMethodCheck(String str) {
		System.out.println("entrato!");
		System.out.println("prima " + str);
		str = str.replaceAll("\\(([^\\)]*)\\)", "()").replaceAll("after#", "").replaceAll("before#", "");
		System.out.println("dopo " + str);
		String[] parts = str.split("\\.");
		boolean found = false;
		String out = "";
		for (int i = 0; i < parts.length; i++) {
			if (!found) {
				if (Character.isUpperCase(parts[i].charAt(0))) {
					found = true;
				}
			} else {
				out = out + "." + parts[i];
			}

		}
		// System.out.println("out = " + out);
		return out.substring(0, out.length() - 2);
	}

	public void checkExistence(String api, String metodo) throws ClassNotFoundException {
		// System.out.println("api "+api);
		metodo = api + "." + metodo;
		if (api.contentEquals("empty")) {
		} else {
			Class<?> cls;
			String checkInner = innerClassCheck(api);
			if (checkInner.equals("")) {
				// System.out.println(api);
				cls = Class.forName(api);
			} else {
				cls = Class.forName(checkInner);
			}
			Method[] metodi = cls.getMethods();

			for (int i = 0; i < metodi.length; ++i) {

				System.out.println(metodi[i]);

			}
		}
	}
}

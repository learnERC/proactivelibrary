package automation;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Refresh {

public static void setFilename(String fileEdit, String aPath, String tmp,  boolean two) throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(fileEdit));
        BufferedWriter bw = new BufferedWriter(new FileWriter(tmp));
        String line;
        boolean stop=true;
        while ((line = br.readLine()) != null ) {
           if (line.contains("FILENAME") && stop==true && two == false) {
        	   line = "public static String FILENAME  = \" " + aPath + " \";";
        	   stop=false;
           }
           bw.append(line+"\n");
           }

        br.close();
        bw.close();
        if(two == false)
        	setFilename(tmp, null, fileEdit, true);


		}

	public static void main(String[] args) {
		// impostare variabile javaService con il path assoluto del file Service.java
		String javaService = "C:\\Users\\danie\\Desktop\\proactivelibrary-iozzia-jacopo-apr2021\\"
				+ "org.eclipse.acceleo.policyenforcergenerator\\src\\org\\eclipse\\acceleo\\policyenforcergenerator\\services\\Service.java";
	    String tmpFileName = "tmp.txt";
	    File f = new File(tmpFileName);
		try {
			setFilename(javaService, args[0], tmpFileName, false);
		} catch (IOException e) {
			e.printStackTrace();
		}
		f.delete();
	    }

	}

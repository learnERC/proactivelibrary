@echo off

set /p Input=your editautomaton: 
copy %Input% %CD%\editautomaton\default.editautomaton 

call mvn clean compile
cd modenforcer

setlocal enabledelayedexpansion

set /p NameLow=your editautomatonName: 
set NameUp=%NameLow%

set "_UCASE=ABCDEFGHIJKLMNOPQRSTUVWXYZ"
set "_LCASE=abcdefghijklmnopqrstuvwxyz"

for /l %%a in (0,1,25) do (
   call set "_FROM=%%_UCASE:~%%a,1%%
   call set "_TO=%%_LCASE:~%%a,1%%
   call set "NameLow=%%NameLow:!_FROM!=!_TO!%%
)

set NameLow

call mvn archetype:generate -DarchetypeGroupId=local.progetto -DarchetypeArtifactId=modenforcer -DarchetypeVersion=1.0 -DgroupId=com.proactive.%NameLow% -DartifactId=%NameUp% -DLowerArtifactId=%NameLow% -DsourceDirectory=%CD%\src-gen -DarchetypeCatalog=local
cd %NameUp%

call mvn validate
call gradle build
call gradle installDebug
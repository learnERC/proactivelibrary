package editautomaton.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import editautomaton.diagram.providers.EditautomatonElementTypes;
import editautomaton.diagram.providers.EditautomatonModelingAssistantProvider;

/**
 * @generated
 */
public class EditautomatonModelingAssistantProviderOfEditAutomatonEditPart
		extends EditautomatonModelingAssistantProvider {

	/**
	* @generated
	*/
	@Override

	public List<IElementType> getTypesForPopupBar(IAdaptable host) {
		List<IElementType> types = new ArrayList<IElementType>(2);
		types.add(EditautomatonElementTypes.InitialState_2001);
		types.add(EditautomatonElementTypes.RegularState_2002);
		return types;
	}

}

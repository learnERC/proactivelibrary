package editautomaton.diagram.providers;

import org.eclipse.gmf.runtime.common.ui.services.icon.IIconProvider;
import org.eclipse.gmf.tooling.runtime.providers.DefaultElementTypeIconProvider;

/**
 * @generated
 */
public class EditautomatonIconProvider extends DefaultElementTypeIconProvider implements IIconProvider {

	/**
	* @generated
	*/
	public EditautomatonIconProvider() {
		super(EditautomatonElementTypes.TYPED_INSTANCE);
	}

}

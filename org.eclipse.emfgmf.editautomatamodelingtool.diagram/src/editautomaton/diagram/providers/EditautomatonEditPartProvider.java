package editautomaton.diagram.providers;

import org.eclipse.gmf.tooling.runtime.providers.DefaultEditPartProvider;

import editautomaton.diagram.edit.parts.EditAutomatonEditPart;
import editautomaton.diagram.edit.parts.EditautomatonEditPartFactory;
import editautomaton.diagram.part.EditautomatonVisualIDRegistry;

/**
 * @generated
 */
public class EditautomatonEditPartProvider extends DefaultEditPartProvider {

	/**
	* @generated
	*/
	public EditautomatonEditPartProvider() {
		super(new EditautomatonEditPartFactory(), EditautomatonVisualIDRegistry.TYPED_INSTANCE,
				EditAutomatonEditPart.MODEL_ID);
	}

}

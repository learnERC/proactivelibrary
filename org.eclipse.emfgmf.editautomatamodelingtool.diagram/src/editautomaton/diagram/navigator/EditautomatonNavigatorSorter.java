package editautomaton.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

import editautomaton.diagram.part.EditautomatonVisualIDRegistry;

/**
 * @generated
 */
public class EditautomatonNavigatorSorter extends ViewerSorter {

	/**
	* @generated
	*/
	private static final int GROUP_CATEGORY = 4003;

	/**
	* @generated
	*/
	public int category(Object element) {
		if (element instanceof EditautomatonNavigatorItem) {
			EditautomatonNavigatorItem item = (EditautomatonNavigatorItem) element;
			return EditautomatonVisualIDRegistry.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}

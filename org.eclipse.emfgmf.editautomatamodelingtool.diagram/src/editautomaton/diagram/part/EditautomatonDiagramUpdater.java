package editautomaton.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;

import editautomaton.EditAutomaton;
import editautomaton.EditautomatonPackage;
import editautomaton.InitialState;
import editautomaton.RegularState;
import editautomaton.State;
import editautomaton.Transition;
import editautomaton.diagram.edit.parts.EditAutomatonEditPart;
import editautomaton.diagram.edit.parts.InitialStateEditPart;
import editautomaton.diagram.edit.parts.RegularStateEditPart;
import editautomaton.diagram.edit.parts.TransitionEditPart;
import editautomaton.diagram.providers.EditautomatonElementTypes;

/**
 * @generated
 */
public class EditautomatonDiagramUpdater {

	/**
	* @generated
	*/
	public static List<EditautomatonNodeDescriptor> getSemanticChildren(View view) {
		switch (EditautomatonVisualIDRegistry.getVisualID(view)) {
		case EditAutomatonEditPart.VISUAL_ID:
			return getEditAutomaton_1000SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<EditautomatonNodeDescriptor> getEditAutomaton_1000SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		EditAutomaton modelElement = (EditAutomaton) view.getElement();
		LinkedList<EditautomatonNodeDescriptor> result = new LinkedList<EditautomatonNodeDescriptor>();
		for (Iterator<?> it = modelElement.getStates().iterator(); it.hasNext();) {
			State childElement = (State) it.next();
			int visualID = EditautomatonVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == InitialStateEditPart.VISUAL_ID) {
				result.add(new EditautomatonNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == RegularStateEditPart.VISUAL_ID) {
				result.add(new EditautomatonNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<EditautomatonLinkDescriptor> getContainedLinks(View view) {
		switch (EditautomatonVisualIDRegistry.getVisualID(view)) {
		case EditAutomatonEditPart.VISUAL_ID:
			return getEditAutomaton_1000ContainedLinks(view);
		case InitialStateEditPart.VISUAL_ID:
			return getInitialState_2001ContainedLinks(view);
		case RegularStateEditPart.VISUAL_ID:
			return getRegularState_2002ContainedLinks(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4001ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<EditautomatonLinkDescriptor> getIncomingLinks(View view) {
		switch (EditautomatonVisualIDRegistry.getVisualID(view)) {
		case InitialStateEditPart.VISUAL_ID:
			return getInitialState_2001IncomingLinks(view);
		case RegularStateEditPart.VISUAL_ID:
			return getRegularState_2002IncomingLinks(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4001IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<EditautomatonLinkDescriptor> getOutgoingLinks(View view) {
		switch (EditautomatonVisualIDRegistry.getVisualID(view)) {
		case InitialStateEditPart.VISUAL_ID:
			return getInitialState_2001OutgoingLinks(view);
		case RegularStateEditPart.VISUAL_ID:
			return getRegularState_2002OutgoingLinks(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4001OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<EditautomatonLinkDescriptor> getEditAutomaton_1000ContainedLinks(View view) {
		EditAutomaton modelElement = (EditAutomaton) view.getElement();
		LinkedList<EditautomatonLinkDescriptor> result = new LinkedList<EditautomatonLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_Transition_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<EditautomatonLinkDescriptor> getInitialState_2001ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<EditautomatonLinkDescriptor> getRegularState_2002ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<EditautomatonLinkDescriptor> getTransition_4001ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<EditautomatonLinkDescriptor> getInitialState_2001IncomingLinks(View view) {
		InitialState modelElement = (InitialState) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<EditautomatonLinkDescriptor> result = new LinkedList<EditautomatonLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Transition_4001(modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<EditautomatonLinkDescriptor> getRegularState_2002IncomingLinks(View view) {
		RegularState modelElement = (RegularState) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<EditautomatonLinkDescriptor> result = new LinkedList<EditautomatonLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Transition_4001(modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<EditautomatonLinkDescriptor> getTransition_4001IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<EditautomatonLinkDescriptor> getInitialState_2001OutgoingLinks(View view) {
		InitialState modelElement = (InitialState) view.getElement();
		LinkedList<EditautomatonLinkDescriptor> result = new LinkedList<EditautomatonLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Transition_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<EditautomatonLinkDescriptor> getRegularState_2002OutgoingLinks(View view) {
		RegularState modelElement = (RegularState) view.getElement();
		LinkedList<EditautomatonLinkDescriptor> result = new LinkedList<EditautomatonLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Transition_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<EditautomatonLinkDescriptor> getTransition_4001OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	private static Collection<EditautomatonLinkDescriptor> getContainedTypeModelFacetLinks_Transition_4001(
			EditAutomaton container) {
		LinkedList<EditautomatonLinkDescriptor> result = new LinkedList<EditautomatonLinkDescriptor>();
		for (Iterator<?> links = container.getTransitions().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Transition) {
				continue;
			}
			Transition link = (Transition) linkObject;
			if (TransitionEditPart.VISUAL_ID != EditautomatonVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			State dst = link.getTarget();
			State src = link.getSource();
			result.add(new EditautomatonLinkDescriptor(src, dst, link, EditautomatonElementTypes.Transition_4001,
					TransitionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<EditautomatonLinkDescriptor> getIncomingTypeModelFacetLinks_Transition_4001(State target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<EditautomatonLinkDescriptor> result = new LinkedList<EditautomatonLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != EditautomatonPackage.eINSTANCE.getTransition_Target()
					|| false == setting.getEObject() instanceof Transition) {
				continue;
			}
			Transition link = (Transition) setting.getEObject();
			if (TransitionEditPart.VISUAL_ID != EditautomatonVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			State src = link.getSource();
			result.add(new EditautomatonLinkDescriptor(src, target, link, EditautomatonElementTypes.Transition_4001,
					TransitionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	* @generated
	*/
	private static Collection<EditautomatonLinkDescriptor> getOutgoingTypeModelFacetLinks_Transition_4001(
			State source) {
		EditAutomaton container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element.eContainer()) {
			if (element instanceof EditAutomaton) {
				container = (EditAutomaton) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<EditautomatonLinkDescriptor> result = new LinkedList<EditautomatonLinkDescriptor>();
		for (Iterator<?> links = container.getTransitions().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Transition) {
				continue;
			}
			Transition link = (Transition) linkObject;
			if (TransitionEditPart.VISUAL_ID != EditautomatonVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			State dst = link.getTarget();
			State src = link.getSource();
			if (src != source) {
				continue;
			}
			result.add(new EditautomatonLinkDescriptor(src, dst, link, EditautomatonElementTypes.Transition_4001,
					TransitionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	* @generated
	*/
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		* @generated
		*/
		@Override

		public List<EditautomatonNodeDescriptor> getSemanticChildren(View view) {
			return EditautomatonDiagramUpdater.getSemanticChildren(view);
		}

		/**
		* @generated
		*/
		@Override

		public List<EditautomatonLinkDescriptor> getContainedLinks(View view) {
			return EditautomatonDiagramUpdater.getContainedLinks(view);
		}

		/**
		* @generated
		*/
		@Override

		public List<EditautomatonLinkDescriptor> getIncomingLinks(View view) {
			return EditautomatonDiagramUpdater.getIncomingLinks(view);
		}

		/**
		* @generated
		*/
		@Override

		public List<EditautomatonLinkDescriptor> getOutgoingLinks(View view) {
			return EditautomatonDiagramUpdater.getOutgoingLinks(view);
		}
	};

}

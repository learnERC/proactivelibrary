package editautomaton.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	* @generated
	*/
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private Messages() {
	}

	/**
	* @generated
	*/
	public static String EditautomatonCreationWizardTitle;

	/**
	* @generated
	*/
	public static String EditautomatonCreationWizard_DiagramModelFilePageTitle;

	/**
	* @generated
	*/
	public static String EditautomatonCreationWizard_DiagramModelFilePageDescription;

	/**
	* @generated
	*/
	public static String EditautomatonCreationWizard_DomainModelFilePageTitle;

	/**
	* @generated
	*/
	public static String EditautomatonCreationWizard_DomainModelFilePageDescription;

	/**
	* @generated
	*/
	public static String EditautomatonCreationWizardOpenEditorError;

	/**
	* @generated
	*/
	public static String EditautomatonCreationWizardCreationError;

	/**
	* @generated
	*/
	public static String EditautomatonCreationWizardPageExtensionError;

	/**
	* @generated
	*/
	public static String EditautomatonDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	* @generated
	*/
	public static String EditautomatonDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	* @generated
	*/
	public static String EditautomatonDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	* @generated
	*/
	public static String EditautomatonDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	* @generated
	*/
	public static String EditautomatonDocumentProvider_isModifiable;

	/**
	* @generated
	*/
	public static String EditautomatonDocumentProvider_handleElementContentChanged;

	/**
	* @generated
	*/
	public static String EditautomatonDocumentProvider_IncorrectInputError;

	/**
	* @generated
	*/
	public static String EditautomatonDocumentProvider_NoDiagramInResourceError;

	/**
	* @generated
	*/
	public static String EditautomatonDocumentProvider_DiagramLoadingError;

	/**
	* @generated
	*/
	public static String EditautomatonDocumentProvider_UnsynchronizedFileSaveError;

	/**
	* @generated
	*/
	public static String EditautomatonDocumentProvider_SaveDiagramTask;

	/**
	* @generated
	*/
	public static String EditautomatonDocumentProvider_SaveNextResourceTask;

	/**
	* @generated
	*/
	public static String EditautomatonDocumentProvider_SaveAsOperation;

	/**
	* @generated
	*/
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	* @generated
	*/
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	* @generated
	*/
	public static String InitDiagramFile_WizardTitle;

	/**
	* @generated
	*/
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	* @generated
	*/
	public static String EditautomatonNewDiagramFileWizard_CreationPageName;

	/**
	* @generated
	*/
	public static String EditautomatonNewDiagramFileWizard_CreationPageTitle;

	/**
	* @generated
	*/
	public static String EditautomatonNewDiagramFileWizard_CreationPageDescription;

	/**
	* @generated
	*/
	public static String EditautomatonNewDiagramFileWizard_RootSelectionPageName;

	/**
	* @generated
	*/
	public static String EditautomatonNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	* @generated
	*/
	public static String EditautomatonNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	* @generated
	*/
	public static String EditautomatonNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	* @generated
	*/
	public static String EditautomatonNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	* @generated
	*/
	public static String EditautomatonNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	* @generated
	*/
	public static String EditautomatonNewDiagramFileWizard_InitDiagramCommand;

	/**
	* @generated
	*/
	public static String EditautomatonNewDiagramFileWizard_IncorrectRootError;

	/**
	* @generated
	*/
	public static String EditautomatonDiagramEditor_SavingDeletedFile;

	/**
	* @generated
	*/
	public static String EditautomatonDiagramEditor_SaveAsErrorTitle;

	/**
	* @generated
	*/
	public static String EditautomatonDiagramEditor_SaveAsErrorMessage;

	/**
	* @generated
	*/
	public static String EditautomatonDiagramEditor_SaveErrorTitle;

	/**
	* @generated
	*/
	public static String EditautomatonDiagramEditor_SaveErrorMessage;

	/**
	* @generated
	*/
	public static String EditautomatonElementChooserDialog_SelectModelElementTitle;

	/**
	* @generated
	*/
	public static String ModelElementSelectionPageMessage;

	/**
	* @generated
	*/
	public static String ValidateActionMessage;

	/**
	* @generated
	*/
	public static String Editautomaton1Group_title;

	/**
	* @generated
	*/
	public static String Transition1CreationTool_title;

	/**
	* @generated
	*/
	public static String Transition1CreationTool_desc;

	/**
	* @generated
	*/
	public static String InitialState2CreationTool_title;

	/**
	* @generated
	*/
	public static String InitialState2CreationTool_desc;

	/**
	* @generated
	*/
	public static String RegularState3CreationTool_title;

	/**
	* @generated
	*/
	public static String RegularState3CreationTool_desc;

	/**
	* @generated
	*/
	public static String CommandName_OpenDiagram;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_EditAutomaton_1000_links;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_InitialState_2001_incominglinks;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_InitialState_2001_outgoinglinks;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_RegularState_2002_incominglinks;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_RegularState_2002_outgoinglinks;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_Transition_4001_target;

	/**
	* @generated
	*/
	public static String NavigatorGroupName_Transition_4001_source;

	/**
	* @generated
	*/
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	* @generated
	*/
	public static String MessageFormatParser_InvalidInputError;

	/**
	* @generated
	*/
	public static String EditautomatonModelingAssistantProviderTitle;

	/**
	* @generated
	*/
	public static String EditautomatonModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}

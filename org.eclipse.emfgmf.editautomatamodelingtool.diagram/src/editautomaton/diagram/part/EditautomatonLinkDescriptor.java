package editautomaton.diagram.part;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.update.UpdaterLinkDescriptor;

/**
 * @generated
 */
public class EditautomatonLinkDescriptor extends UpdaterLinkDescriptor {
	/**
	* @generated
	*/
	public EditautomatonLinkDescriptor(EObject source, EObject destination, IElementType elementType, int linkVID) {
		super(source, destination, elementType, linkVID);
	}

	/**
	* @generated
	*/
	public EditautomatonLinkDescriptor(EObject source, EObject destination, EObject linkElement,
			IElementType elementType, int linkVID) {
		super(source, destination, linkElement, elementType, linkVID);
	}

}

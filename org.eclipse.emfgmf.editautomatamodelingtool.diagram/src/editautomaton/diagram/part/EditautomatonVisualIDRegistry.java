package editautomaton.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;

import editautomaton.EditAutomaton;
import editautomaton.EditautomatonPackage;
import editautomaton.diagram.edit.parts.EditAutomatonEditPart;
import editautomaton.diagram.edit.parts.InitialStateEditPart;
import editautomaton.diagram.edit.parts.InitialStateNameEditPart;
import editautomaton.diagram.edit.parts.RegularStateEditPart;
import editautomaton.diagram.edit.parts.RegularStateNameEditPart;
import editautomaton.diagram.edit.parts.TransitionActionToPerformEditPart;
import editautomaton.diagram.edit.parts.TransitionEditPart;
import editautomaton.diagram.edit.parts.TransitionInterceptedActionEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class EditautomatonVisualIDRegistry {

	/**
	* @generated
	*/
	private static final String DEBUG_KEY = "org.eclipse.emfgmf.editautomatamodelingtool.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	* @generated
	*/
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (EditAutomatonEditPart.MODEL_ID.equals(view.getType())) {
				return EditAutomatonEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return editautomaton.diagram.part.EditautomatonVisualIDRegistry.getVisualID(view.getType());
	}

	/**
	* @generated
	*/
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	* @generated
	*/
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(Platform.getDebugOption(DEBUG_KEY))) {
				EditautomatonDiagramEditorPlugin.getInstance()
						.logError("Unable to parse view type as a visualID number: " + type);
			}
		}
		return -1;
	}

	/**
	* @generated
	*/
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	* @generated
	*/
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (EditautomatonPackage.eINSTANCE.getEditAutomaton().isSuperTypeOf(domainElement.eClass())
				&& isDiagram((EditAutomaton) domainElement)) {
			return EditAutomatonEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	* @generated
	*/
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = editautomaton.diagram.part.EditautomatonVisualIDRegistry.getModelID(containerView);
		if (!EditAutomatonEditPart.MODEL_ID.equals(containerModelID)) {
			return -1;
		}
		int containerVisualID;
		if (EditAutomatonEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = editautomaton.diagram.part.EditautomatonVisualIDRegistry.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = EditAutomatonEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case EditAutomatonEditPart.VISUAL_ID:
			if (EditautomatonPackage.eINSTANCE.getInitialState().isSuperTypeOf(domainElement.eClass())) {
				return InitialStateEditPart.VISUAL_ID;
			}
			if (EditautomatonPackage.eINSTANCE.getRegularState().isSuperTypeOf(domainElement.eClass())) {
				return RegularStateEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	* @generated
	*/
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = editautomaton.diagram.part.EditautomatonVisualIDRegistry.getModelID(containerView);
		if (!EditAutomatonEditPart.MODEL_ID.equals(containerModelID)) {
			return false;
		}
		int containerVisualID;
		if (EditAutomatonEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = editautomaton.diagram.part.EditautomatonVisualIDRegistry.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = EditAutomatonEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case EditAutomatonEditPart.VISUAL_ID:
			if (InitialStateEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RegularStateEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case InitialStateEditPart.VISUAL_ID:
			if (InitialStateNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RegularStateEditPart.VISUAL_ID:
			if (RegularStateNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TransitionEditPart.VISUAL_ID:
			if (TransitionInterceptedActionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (TransitionActionToPerformEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	* @generated
	*/
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (EditautomatonPackage.eINSTANCE.getTransition().isSuperTypeOf(domainElement.eClass())) {
			return TransitionEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	* User can change implementation of this method to handle some specific
	* situations not covered by default logic.
	* 
	* @generated
	*/
	private static boolean isDiagram(EditAutomaton element) {
		return true;
	}

	/**
	* @generated
	*/
	public static boolean checkNodeVisualID(View containerView, EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	* @generated
	*/
	public static boolean isCompartmentVisualID(int visualID) {
		return false;
	}

	/**
	* @generated
	*/
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case EditAutomatonEditPart.VISUAL_ID:
			return false;
		case InitialStateEditPart.VISUAL_ID:
		case RegularStateEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	* @generated
	*/
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		* @generated
		*/
		@Override

		public int getVisualID(View view) {
			return editautomaton.diagram.part.EditautomatonVisualIDRegistry.getVisualID(view);
		}

		/**
		* @generated
		*/
		@Override

		public String getModelID(View view) {
			return editautomaton.diagram.part.EditautomatonVisualIDRegistry.getModelID(view);
		}

		/**
		* @generated
		*/
		@Override

		public int getNodeVisualID(View containerView, EObject domainElement) {
			return editautomaton.diagram.part.EditautomatonVisualIDRegistry.getNodeVisualID(containerView,
					domainElement);
		}

		/**
		* @generated
		*/
		@Override

		public boolean checkNodeVisualID(View containerView, EObject domainElement, int candidate) {
			return editautomaton.diagram.part.EditautomatonVisualIDRegistry.checkNodeVisualID(containerView,
					domainElement, candidate);
		}

		/**
		* @generated
		*/
		@Override

		public boolean isCompartmentVisualID(int visualID) {
			return editautomaton.diagram.part.EditautomatonVisualIDRegistry.isCompartmentVisualID(visualID);
		}

		/**
		* @generated
		*/
		@Override

		public boolean isSemanticLeafVisualID(int visualID) {
			return editautomaton.diagram.part.EditautomatonVisualIDRegistry.isSemanticLeafVisualID(visualID);
		}
	};

}

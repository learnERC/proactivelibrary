/**
 */
package editautomaton.util;

import editautomaton.*;

import java.util.Map;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import org.eclipse.emf.ecore.xml.type.util.XMLTypeUtil;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see editautomaton.EditautomatonPackage
 * @generated
 */
public class EditautomatonValidator extends EObjectValidator {
	private boolean after = false;
	private String afterActionSignature;
	
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final EditautomatonValidator INSTANCE = new EditautomatonValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "editautomaton";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditautomatonValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return EditautomatonPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {

		switch (classifierID) {
			case EditautomatonPackage.EDIT_AUTOMATON:
				return validateEditAutomaton((EditAutomaton)value, diagnostics, context);
			case EditautomatonPackage.STATE:
				return validateState((State)value, diagnostics, context);
			case EditautomatonPackage.TRANSITION:
				return validateTransition((Transition)value, diagnostics, context);
			case EditautomatonPackage.INITIAL_STATE:
				return validateInitialState((InitialState)value, diagnostics, context);
			case EditautomatonPackage.REGULAR_STATE:
				return validateRegularState((RegularState)value, diagnostics, context);
			case EditautomatonPackage.ACTION_SIGNATURE:
				return validateActionSignature((String)value, diagnostics, context);
			case EditautomatonPackage.ACTION_SEQUENCE:
				return validateActionSequence((String)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEditAutomaton(EditAutomaton editAutomaton, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(editAutomaton, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateState(State state, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(state, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransition(Transition transition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInitialState(InitialState initialState, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(initialState, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRegularState(RegularState regularState, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(regularState, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateActionSignature(String actionSignature, DiagnosticChain diagnostics, Map<Object, Object> context) {		
		boolean result = validateActionSignature_Pattern(actionSignature, diagnostics, context);
		try {
			result = result && checkSignature(actionSignature);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		if(result && actionSignature.contains("after#")) {
			after = true;
			afterActionSignature = actionSignature;
		} else {
			after = false;
		}
		
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 * @see #validateActionSignature_Pattern
	 */
	public static final  PatternMatcher [][] ACTION_SIGNATURE__PATTERN__VALUES =
		new PatternMatcher [][] {
			new PatternMatcher [] {
				XMLTypeUtil.createPatternMatcher("(after#|before#).*")
			}
		};

	/**
	 * Validates the Pattern constraint of '<em>Action Signature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateActionSignature_Pattern(String actionSignature, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validatePattern(EditautomatonPackage.Literals.ACTION_SIGNATURE, actionSignature, ACTION_SIGNATURE__PATTERN__VALUES, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateActionSequence(String actionSequence, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateActionSequence_Pattern(actionSequence, diagnostics, context);
		
		if(after && !actionSequence.startsWith(afterActionSignature))
			return false;
		
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 * @see #validateActionSequence_Pattern
	 */
	public static final  PatternMatcher [][] ACTION_SEQUENCE__PATTERN__VALUES =
		new PatternMatcher [][] {
			new PatternMatcher [] {
				XMLTypeUtil.createPatternMatcher("((after#|before#)[^;]*;?.*)*")
			}
		};

	/**
	 * Validates the Pattern constraint of '<em>Action Sequence</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateActionSequence_Pattern(String actionSequence, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validatePattern(EditautomatonPackage.Literals.ACTION_SEQUENCE, actionSequence, ACTION_SEQUENCE__PATTERN__VALUES, diagnostics, context);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}
	
	public static boolean checkSignature(String value) throws ClassNotFoundException{
		System.out.println("actionSignature: " + value);
		value = value.replace("after#", "").replace("before#", "").replaceAll("\\w+\\s*=\\s*", "");
		System.out.println("actionSignature after replace: " + value);
		String[] actionSignatures = value.split(";");
		
		boolean[] checks = new boolean[actionSignatures.length];
		for(int j = 0; j<actionSignatures.length; j++){
			checks[j] = false;
		}
		int index = 0;
		
		for (String actionSignature: actionSignatures){			
			if(actionSignatures.length >= 2){
				System.out.println("Signature: " + actionSignature);
			}
			if(actionSignature.contains("new ")){
				try{
					String api = actionSignature.replace("new ", "").substring(0, actionSignature.replace("new ", "").indexOf("(")); //.replace("(","").replace(")","");
					String constructor = actionSignature.replace("new ", "").replaceAll(" ", "");
					System.out.println("api: " + api );
					System.out.println("constructor: " + constructor);
					Class<?> clazz = Class.forName(api);
					Constructor<?>[] constructors = clazz.getConstructors();

					for(Constructor<?> cons: constructors){
						String consString = cons.toString();
						//System.out.println(consString);
						if(consString.contains(constructor)){
							//System.out.println("Trovato costruttore!");
							checks[index] = true;
						}
					}
					
					if(!checks[index]) {
						System.out.println("Costruttore non trovato !");
					}
				}catch(Exception e){
					System.out.println("Costruttore non trovato !");
				}
				index++;
				
			}else if (actionSignature.contains("Enforcer")){
				//System.out.println("Trovato enforcer: " + actionSignature);
				checks[index] = true;
				index++;
				
			}else{
				try {
					boolean foundUpperCase = false;
					int endApi = 0;
					for (int i = 0; i<actionSignature.length(); i++){
						if(Character.isUpperCase(actionSignature.charAt(i))){
							foundUpperCase = true;
						}
						if(foundUpperCase){
							if(actionSignature.charAt(i) == '.'){
								endApi = i;
								break;
							}
						}
					}
					
					String api = actionSignature.substring(0,endApi).replace("(", "").replace(")", "").replaceAll(" ", "");
					String method = actionSignature.substring(endApi+1 , actionSignature.length()).replaceAll(" ", "");

					//System.out.println("api: " + api);
					//System.out.println("method: " + method);

					Class<?> clazz = Class.forName(api);
					Method[] methods = clazz.getDeclaredMethods();
					
					for(Method met: methods){
						String metString = met.toString();
						//System.out.println(metString);
						if(metString.contains(method)){
							//System.out.println("Trovato metodo!");
							checks[index] = true;
						}
					}
				}catch(Exception e){
					System.out.println("Classe non trovata !");
				}
				if(!checks[index]){
					System.out.println("Metodo non trovato !");
				}
				index++;
			}		
		}
		
		boolean result = true;
		for(boolean check: checks){
			result = result && check;
		}
		System.out.println("\n");
		return result;
	}
} //EditautomatonValidator

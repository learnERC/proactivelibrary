/**
 */
package editautomaton;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Edit Automaton</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link editautomaton.EditAutomaton#getStates <em>States</em>}</li>
 *   <li>{@link editautomaton.EditAutomaton#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link editautomaton.EditAutomaton#getName <em>Name</em>}</li>
 *   <li>{@link editautomaton.EditAutomaton#getHandledObject <em>Handled Object</em>}</li>
 *   <li>{@link editautomaton.EditAutomaton#getApi <em>Api</em>}</li>
 * </ul>
 *
 * @see editautomaton.EditautomatonPackage#getEditAutomaton()
 * @model
 * @generated
 */
public interface EditAutomaton extends EObject {
	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link editautomaton.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see editautomaton.EditautomatonPackage#getEditAutomaton_States()
	 * @model containment="true"
	 * @generated
	 */
	EList<State> getStates();

	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link editautomaton.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see editautomaton.EditautomatonPackage#getEditAutomaton_Transitions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Transition> getTransitions();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see editautomaton.EditautomatonPackage#getEditAutomaton_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link editautomaton.EditAutomaton#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Handled Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Handled Object</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Handled Object</em>' attribute.
	 * @see #setHandledObject(String)
	 * @see editautomaton.EditautomatonPackage#getEditAutomaton_HandledObject()
	 * @model
	 * @generated
	 */
	String getHandledObject();

	/**
	 * Sets the value of the '{@link editautomaton.EditAutomaton#getHandledObject <em>Handled Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Handled Object</em>' attribute.
	 * @see #getHandledObject()
	 * @generated
	 */
	void setHandledObject(String value);

	/**
	 * Returns the value of the '<em><b>Api</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Api</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Api</em>' attribute.
	 * @see #setApi(String)
	 * @see editautomaton.EditautomatonPackage#getEditAutomaton_Api()
	 * @model
	 * @generated
	 */
	String getApi();

	/**
	 * Sets the value of the '{@link editautomaton.EditAutomaton#getApi <em>Api</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Api</em>' attribute.
	 * @see #getApi()
	 * @generated
	 */
	void setApi(String value);

} // EditAutomaton

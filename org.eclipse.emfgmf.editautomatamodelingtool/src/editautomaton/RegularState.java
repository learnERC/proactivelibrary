/**
 */
package editautomaton;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Regular State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see editautomaton.EditautomatonPackage#getRegularState()
 * @model
 * @generated
 */
public interface RegularState extends State {
} // RegularState

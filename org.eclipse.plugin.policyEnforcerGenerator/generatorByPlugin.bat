@echo off

set Input=C:\AmbienteDiSviluppoTesi\runtime-EclipseApplication\CameraHandlerEditautomaton\default.editautomaton
set PathGenerator=C:\AmbienteDiSviluppoTesi\WorkspaceNeon\org.eclipse.plugin.policyEnforcerGenerator
copy %Input% %PathGenerator%\editautomaton\default.editautomaton 

echo PROVA del change direcotry -C:\AmbienteDiSviluppoTesi\WorkspaceNeon\org.eclipse.plugin.policyEnforcerGenerator
cd C:\AmbienteDiSviluppoTesi\WorkspaceNeon\org.eclipse.plugin.policyEnforcerGeneratorcall mvn clean compile
echo C:\AmbienteDiSviluppoTesi\WorkspaceNeon\org.eclipse.plugin.policyEnforcerGenerator\modenforcer
cd C:\AmbienteDiSviluppoTesi\WorkspaceNeon\org.eclipse.plugin.policyEnforcerGenerator\modenforcer 

setlocal enabledelayedexpansion

set NameLow=CameraReleaseEnforcer
set NameUp=%NameLow%

set "_UCASE=ABCDEFGHIJKLMNOPQRSTUVWXYZ"
set "_LCASE=abcdefghijklmnopqrstuvwxyz"

for /l %%a in (0,1,25) do (
	call set "_FROM=%%_UCASE:~%%a,1%%
	call set "_TO=%%_LCASE:~%%a,1%%
	call set "NameLow=%%NameLow:!_FROM!=!_TO!%%
)

set NameLow

call mvn archetype:generate -DarchetypeGroupId=local.progetto -DarchetypeArtifactId=modenforcer -DarchetypeVersion=1.0 -DgroupId=com.proactive.%NameLow% -DartifactId=%NameUp% -DLowerArtifactId=%NameLow% -DsourceDirectory=%PathGenerator%\src-gen -DarchetypeCatalog=local
copy %PathGenerator%\src-gen\%NameUp%.java %NameUp%\app\src\main\java\com\proactive\%NameLow%\%NameUp%.java

echo %NameUp%
cd %NameUp%

call mvn validate
call gradle build
call gradle installDebug
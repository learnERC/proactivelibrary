package org.eclipse.plugin.policyenforcergenerator.popup.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import policyenforcergenerator.gui.GraphicUserInterface;

public class GeneraEnforcer implements IObjectActionDelegate {

	private Shell shell;
	
	/**
	 * Constructor for Action1.
	 */
	public GeneraEnforcer() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		IWorkbenchWindow  window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		IPath modelPath = null;
		String generatorPath = null;
	    if (window != null)
	    {
	        IStructuredSelection selection = (IStructuredSelection) window.getSelectionService().getSelection();
	        Object firstElement = selection.getFirstElement();
	        if (firstElement instanceof IAdaptable)
	        {
	        	modelPath = ((IAdaptable) firstElement).getAdapter(IResource.class).getLocation();
	        }
	    }
	    
	    System.out.println(modelPath.toString());
	    String modelPathstr = modelPath.toString().replace("/", File.separator);
	    System.out.println(modelPathstr);
	    
	    try {
			InputStream inputStream = new FileInputStream(System.getProperty("user.home") + File.separator + "proactiveModulesCache.properties");
			Properties prop = new Properties();
			prop.load(inputStream);
			generatorPath = prop.getProperty("policyEnforcerGeneratorPath");
		} catch (IOException e) {
			generatorPath = new File("").getAbsolutePath(); //ResourcesPlugin.getWorkspace().getRoot().getLocation().toString();
		}
	    
	    System.out.println(generatorPath);
	    String generatorPathstr = generatorPath.replace("/", File.separator);
	    System.out.println(generatorPathstr);
	    GraphicUserInterface gui = new GraphicUserInterface(modelPathstr, generatorPathstr);
	    gui.setVisible(true);
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}

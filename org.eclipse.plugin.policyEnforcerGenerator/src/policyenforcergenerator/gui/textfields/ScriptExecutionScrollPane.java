package policyenforcergenerator.gui.textfields;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;

import policyenforcergenerator.gui.GraphicUserInterface;
import policyenforcergenerator.utilities.ScriptExecutor;

public class ScriptExecutionScrollPane extends JScrollPane{
	private JTextArea textArea;
	private GraphicUserInterface gui;
	private String content;
	
	public ScriptExecutionScrollPane(GraphicUserInterface gui, JTextArea textArea, String content, int x, int y, int width, int height, boolean isEnabled) {
		super(textArea);
		this.textArea = textArea;
		this.textArea.setEditable(false);
		this.setFont(new Font("Aerial",Font.BOLD, 12));
		
		Insets insets = gui.getInsets();
		this.setBounds(x + insets.left, y + insets.top,
	             width, height);
		
		this.content = content;
		this.gui = gui;
		
		
		textArea = new JTextArea(20,30);
		textArea.setEnabled(isEnabled);
		this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		gui.add(this);
		//gui.add(this);
		this.getViewport().getView().setEnabled(isEnabled);

		
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public JTextArea getTextArea() {
		return textArea;
	}

	public void setTextArea(JTextArea textArea) {
		this.textArea = textArea;
	}

	public GraphicUserInterface getGui() {
		return gui;
	}
	
}

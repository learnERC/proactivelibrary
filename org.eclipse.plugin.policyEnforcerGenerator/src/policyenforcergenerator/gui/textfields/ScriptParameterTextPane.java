package policyenforcergenerator.gui.textfields;

import java.awt.Font;
import java.awt.Insets;

import javax.swing.JTextArea;
import javax.swing.JTextPane;

import policyenforcergenerator.gui.GraphicUserInterface;

public class ScriptParameterTextPane extends JTextArea{
	private GraphicUserInterface gui;
	private String textAreaName;
	
	public ScriptParameterTextPane(GraphicUserInterface gui,String content, String textAreaName, int x, int y, int height, int width, boolean isEnabled){
		this.setText(content);
		this.setFont(new Font("Aerial",Font.BOLD, height/2));
		this.textAreaName = textAreaName;
		Insets insets = gui.getInsets();
		if(content != null && content != "" && textAreaName != "version"){
			/*this.setBounds(x + insets.left, y + insets.top,
					(int) (content.length() * height/2), height);*/
			this.setBounds(x + insets.left, y + insets.top,
					(int) (width), height);
		}else if (textAreaName == "version") {
			this.setBounds(x + insets.left, y + insets.top,
		             width, height);
		} else {
			this.setBounds(x + insets.left, y + insets.top,
		             width, height*2);
		}
		
		
		this.setColumns(1);
		this.setRows(1);
		
		this.gui = gui;
		gui.add(this);
		this.setEnabled(isEnabled);
		
	}
	
	public String getTextAreaName() {
		return textAreaName;
	}

	public void setTextAreaName(String textAreaName) {
		this.textAreaName = textAreaName;
	}
	
}

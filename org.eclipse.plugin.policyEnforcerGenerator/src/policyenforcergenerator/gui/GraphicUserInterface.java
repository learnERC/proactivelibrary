package policyenforcergenerator.gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.*;

import org.eclipse.core.runtime.IPath;

import policyenforcergenerator.gui.buttons.ConfirmParametersButton;
import policyenforcergenerator.gui.buttons.FileExplorerButton;
import policyenforcergenerator.gui.buttons.InstallerButton;
import policyenforcergenerator.gui.textfields.ScriptExecutionScrollPane;
import policyenforcergenerator.gui.textfields.ScriptParameterTextPane;
import policyenforcergenerator.utilities.ScriptExecutor;

public class GraphicUserInterface extends JFrame{
	private Dimension screenSize;
	private ArrayList<FileExplorerButton> paramButtons;
	private ConfirmParametersButton confirmButton;
	private InstallerButton installerButton;
	private JTextArea version;
	private JTextArea description;
	private ScriptExecutionScrollPane execArea;
	private int width;
	
	public GraphicUserInterface(String modelPath, String generatorPath){
		super("Proactive Module Generation");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height = screenSize.height;
        int width = screenSize.width;
        this.width = (int) (width/1.5);
        this.setSize(this.width, (int) (height/1.1));
        
        paramButtons = new ArrayList<FileExplorerButton>();
        confirmButton = null;
       
        int x = 25 ,y = 5;
        initModelComponents("Select Edit Automaton", "editautomaton", modelPath, x, y);
        initGeneratorComponents("Select Proactive Module Generator", "", generatorPath);        
        initVersion();
        initDescription();
        initScirptComponents("Generate Proactive Module", " Deploy Proactive Module");
        
        int h1 = 0;
        
        for (int i = 0; i<paramButtons.size();i++)
        	h1 += paramButtons.get(i).getHeight();
	    
        h1 += confirmButton.getHeight();
        h1 += version.getHeight();
        h1 += description.getHeight();
        this.setSize((int) (width/1.5), (int) (h1*1.87));
        this.setLocationRelativeTo(null);
        this.setLayout(null);
        this.setResizable(false);
	}
	
	public Dimension getScreenSize() {
		return screenSize;
	}

	public void setScreenSize(Dimension screenSize) {
		this.screenSize = screenSize;
	}

	public ArrayList<FileExplorerButton> getButtons() {
		return paramButtons;
	}

	public void setButtons(ArrayList<FileExplorerButton> buttons) {
		this.paramButtons = buttons;
	}

	public JTextArea getVersion() {
		return version;
	}
	
	public JTextArea getDescription() {
		return description;
	}

	public void setVersion(JTextArea version) {
		this.version = version;
	}

	public ArrayList<FileExplorerButton> getParamButtons() {
		return paramButtons;
	}

	public void setParamButtons(ArrayList<FileExplorerButton> paramButtons) {
		this.paramButtons = paramButtons;
	}

	public ConfirmParametersButton getConfirmButton() {
		return confirmButton;
	}

	public void setConfirmButton(ConfirmParametersButton confirmButton) {
		this.confirmButton = confirmButton;
	}

	public InstallerButton getInstallerButton() {
		return installerButton;
	}

	public void setInstallerButton(InstallerButton installerButton) {
		this.installerButton = installerButton;
	}

	public ScriptExecutionScrollPane getExecArea() {
		return execArea;
	}

	public void setExecArea(ScriptExecutionScrollPane execArea) {
		this.execArea = execArea;
	}
	
	private void initModelComponents(String buttonName, String fileExtesion, String path, int x, int y){
		FileExplorerButton button = new FileExplorerButton(this, buttonName, path, fileExtesion, x, y);	
		
		ScriptParameterTextPane textArea = new ScriptParameterTextPane(this, button.getPathSelected(), button.getPathSelected(), button.getX()+button.getWidth() + 25, y, 
				button.getHeight(), (int) (this.width/1.4), false);
		
		button.setTextArea(textArea);
		paramButtons.add(button);
	}
	
	private void initGeneratorComponents(String buttonName, String fileExtesion, String path){
		FileExplorerButton button = new FileExplorerButton(this, buttonName, path,  fileExtesion, 
				this.paramButtons.get(0).getX(), this.paramButtons.get(0).getY() + ((int) ((this.paramButtons.get(0).getHeight() * 1.8))));
		
		ScriptParameterTextPane textArea = new ScriptParameterTextPane(this, button.getPathSelected(), button.getPathSelected(), 
				this.paramButtons.get(0).getX() + ((int)((this.paramButtons.get(0).getWidth() * 1.1))), 
				this.paramButtons.get(0).getY() + ((int) ((this.paramButtons.get(0).getHeight() * 1.8))), this.paramButtons.get(0).getHeight(), (int) (this.width/1.4), false);
		
		
		button.setTextArea(textArea);
		paramButtons.add(button);
	}
	
	private void initVersion(){
		JLabel versionLabel = new JLabel("Proactive Module Version");
		versionLabel.setBounds(this.paramButtons.get(1).getX(), this.paramButtons.get(1).getY() + ((int) ((this.paramButtons.get(1).getHeight() * 1.8))), 
				this.paramButtons.get(1).getWidth(), this.paramButtons.get(1).getHeight());
		
		this.add(versionLabel);
		
		this.version = new ScriptParameterTextPane(this, "1.0", "version", this.paramButtons.get(1).getX() + ((int)((this.paramButtons.get(1).getWidth() * 1.1))), 
				this.paramButtons.get(1).getY() + ((int) ((this.paramButtons.get(1).getHeight() * 1.8))), this.paramButtons.get(1).getHeight(), (int) (this.width/1.4), true);
	}
	
	private void initDescription(){
		JLabel descriptionLabel = new JLabel("Proactive Module Description");
		descriptionLabel.setBounds(this.paramButtons.get(1).getX(), this.version.getY() + ((int) ((this.version.getHeight() * 1.8))), 
				this.version.getWidth(), this.version.getHeight());
		
		this.add(descriptionLabel);
		JTextArea ta = new ScriptParameterTextPane(this, "", "description", this.paramButtons.get(1).getX() + ((int)((this.paramButtons.get(1).getWidth() * 1.1))), 
				this.version.getY() + ((int) ((this.version.getHeight() * 1.8))), this.version.getHeight(), (int) (this.width/1.4), true);
		ta.setLineWrap(true);
		
		this.description = ta;

	}
	
	private void initScirptComponents(String buttonName, String buttonName2){
		
		ConfirmParametersButton button = new ConfirmParametersButton(this, buttonName, null, 
				this.paramButtons.get(0).getX() + ((int)((this.paramButtons.get(0).getWidth() * 1.2))),
				this.description.getY() + ((int) ((this.description.getHeight() * 1.5))));
		
		confirmButton = button;
		
		ScriptExecutionScrollPane textArea = new ScriptExecutionScrollPane(this, new JTextArea(), "", this.paramButtons.get(0).getX(), 
				this.confirmButton.getY() + ((int) ((this.confirmButton.getHeight() * 1.8))), 
				(this.getWidth() - this.paramButtons.get(0).getX()*2), (this.getHeight() - (this.confirmButton.getY() *2) + this.paramButtons.get(0).getY()), true);
		
		confirmButton.setTextArea(textArea);
		execArea = textArea;
		
		InstallerButton button2 = new InstallerButton(this, buttonName2, null, 
				this.paramButtons.get(0).getX() + ((int)((this.paramButtons.get(0).getWidth() * 2.4))), 
				this.description.getY() + ((int) ((this.description.getHeight() * 1.5))));
		
		installerButton = button2;
		
	}
	
	public void setMaxFrameHeight(){
		this.setPreferredSize(new Dimension(this.getWidth(), (int) (screenSize.height/1.25)));
		this.setMinimumSize(new Dimension(this.getWidth(), (int) (screenSize.height/1.25)));
		this.setSize(this.getWidth(), (int) (screenSize.height/1.25));
	}
		
}

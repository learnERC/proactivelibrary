package policyenforcergenerator.gui.buttons;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import policyenforcergenerator.gui.GraphicUserInterface;
import policyenforcergenerator.gui.textfields.ScriptExecutionScrollPane;
import policyenforcergenerator.utilities.ModelSelection;
import policyenforcergenerator.utilities.ScriptExecutor;
import policyenforcergenerator.utilities.ScriptGenerator;

public class InstallerButton extends JButton{
	private GraphicUserInterface gui;
	private ScriptExecutionScrollPane textArea;
	private String descButton;
	private ArrayList<String> parameters;
	
	public InstallerButton(GraphicUserInterface gui, String buttonName, ArrayList<String> params, int x,
			int y) {
		super(buttonName);
		descButton = buttonName;
		this.parameters = params;
		this.textArea = gui.getExecArea();

		Insets insets = gui.getInsets();
		Dimension size = this.getPreferredSize();
		this.setBounds(x + insets.left, y + insets.top, 250, size.height);
		this.gui = gui;
		this.setEnabled(false);
		gui.add(this);

		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onClick();

			}
		});
	}

	public void onClick() {

		int dialogButton = JOptionPane.YES_NO_OPTION;
		dialogButton = JOptionPane.showConfirmDialog(null,
				"Do you want to install the generated Proactive Module on connected android devices?", "Warning", dialogButton);

		if (dialogButton == JOptionPane.YES_OPTION) { // The ISSUE is here
			System.out.println("OPZIONE SI");
			this.setParameters();
			ScriptGenerator scriptGenerator = new ScriptGenerator(parameters.get(0),
					ModelSelection.extractModelName(parameters.get(0)), parameters.get(1), parameters.get(2), parameters.get(3));
			ScriptExecutor scriptExecutor = new ScriptExecutor(parameters.get(1), scriptGenerator.getProjectGeneratorScriptName(),
					scriptGenerator.getProjectInstallerScriptName(), textArea);
			
			try {
				scriptExecutor.runScriptInstaller();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// scriptGenerator.execScript();

		} else {
			System.out.println("OPZIONE NO");
		}
	}

	public ScriptExecutionScrollPane getTextArea() {
		return textArea;
	}

	public void setTextArea(ScriptExecutionScrollPane textArea) {
		this.textArea = textArea;
	}

	public String getDescButton() {
		return descButton;
	}

	public void setDescButton(String descButton) {
		this.descButton = descButton;
	}

	public ArrayList<String> getParameters() {
		return parameters;
	}

	public void setParameters() {
		parameters = new ArrayList<String>();
		for (int i = 0; i < gui.getButtons().size(); i++) {
			parameters.add(gui.getButtons().get(i).getTextArea().getText());
		}

		parameters.add(gui.getVersion().getText());
		parameters.add(gui.getDescription().getText());
	}

	public void setParameters(ArrayList<String> parameters) {
		this.parameters = parameters;
	}
}

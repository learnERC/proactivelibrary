package policyenforcergenerator.gui.buttons;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.eclipse.core.resources.ResourcesPlugin;

import policyenforcergenerator.gui.GraphicUserInterface;
import policyenforcergenerator.gui.textfields.ScriptExecutionScrollPane;
import policyenforcergenerator.gui.textfields.ScriptParameterTextPane;
import policyenforcergenerator.utilities.ModelSelection;
import policyenforcergenerator.utilities.ScriptExecutor;
import policyenforcergenerator.utilities.ScriptGenerator;

public class ConfirmParametersButton extends JButton {
	private GraphicUserInterface gui;
	private ScriptExecutionScrollPane textArea;
	private String descButton;
	private ArrayList<String> parameters;

	public ConfirmParametersButton(GraphicUserInterface gui, String buttonName, ArrayList<String> params, int x,
			int y) {
		super(buttonName);
		descButton = buttonName;
		this.parameters = params;
		textArea = null;

		Insets insets = gui.getInsets();
		Dimension size = this.getPreferredSize();
		this.setBounds(x + insets.left, y + insets.top, 250, size.height);
		this.gui = gui;
		gui.add(this);

		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onClick();
				String path = gui.getButtons().get(1).getTextArea().getText() + File.separator + "modenforcer" + File.separator + ModelSelection.extractModelName(parameters.get(0));
				File dir = new File(path);
				if (dir.exists()) {
					gui.getInstallerButton().setEnabled(true);
				}

			}
		});
	}

	public void onClick() {
		
		

		int dialogButton = JOptionPane.YES_NO_OPTION;
		dialogButton = JOptionPane.showConfirmDialog(null,
				"Do you want to generate the Proactive Module with the selected parameters?", "Warning", dialogButton);

		if (dialogButton == JOptionPane.YES_OPTION) { // The ISSUE is here
			System.out.println("OPZIONE SI");
			
			this.setParameters();
			try {
			String homePath = System.getProperty("user.home") + File.separator + "proactiveModulesCache.properties";
			PrintWriter peg_writer = new PrintWriter(homePath, "UTF-8");
			peg_writer.println("policyEnforcerGeneratorPath=" + parameters.get(1).replace("\\", "\\\\"));
			peg_writer.close();
			
			
			String propPath = parameters.get(1) + File.separator + "config.properties";
			PrintWriter writer = new PrintWriter(propPath, "UTF-8");
			writer.println("editautomatonPath=" + parameters.get(0).replace("\\", "\\\\"));
			writer.close();
			} catch(IOException e) {}
			
			ScriptGenerator scriptGenerator = null;
			
			try {
			scriptGenerator = new ScriptGenerator(parameters.get(0),
					ModelSelection.extractModelName(parameters.get(0)), parameters.get(1), parameters.get(2), parameters.get(3));
			} catch (StringIndexOutOfBoundsException e) {
				JOptionPane.showMessageDialog(null, "The editAutomaton must have a valid name!");
			}
			
			ScriptExecutor scriptExecutor = new ScriptExecutor(parameters.get(1), scriptGenerator.getProjectGeneratorScriptName(),
					scriptGenerator.getProjectInstallerScriptName(), textArea);
			
			try {
				scriptExecutor.runScriptGenerator();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// scriptGenerator.execScript();
				

		} else {
			System.out.println("OPZIONE NO");
		}

	}

	public ScriptExecutionScrollPane getTextArea() {
		return textArea;
	}

	public void setTextArea(ScriptExecutionScrollPane textArea) {
		this.textArea = textArea;
	}

	public String getDescButton() {
		return descButton;
	}

	public void setDescButton(String descButton) {
		this.descButton = descButton;
	}

	public ArrayList<String> getParameters() {
		return parameters;
	}

	public void setParameters() {
		parameters = new ArrayList<String>();
		for (int i = 0; i < gui.getButtons().size(); i++) {
			parameters.add(gui.getButtons().get(i).getTextArea().getText());
		}

		parameters.add(gui.getVersion().getText());
		parameters.add(gui.getDescription().getText());
	}

	public void setParameters(ArrayList<String> parameters) {
		this.parameters = parameters;
	}

}

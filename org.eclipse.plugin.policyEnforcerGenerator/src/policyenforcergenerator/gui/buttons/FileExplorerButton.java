package policyenforcergenerator.gui.buttons;


import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.eclipse.core.resources.ResourcesPlugin;

import policyenforcergenerator.gui.GraphicUserInterface;
import policyenforcergenerator.gui.textfields.ScriptParameterTextPane;
import policyenforcergenerator.utilities.ModelSelection;

public class FileExplorerButton extends JButton{
	private GraphicUserInterface gui;
	private ScriptParameterTextPane textArea;
	private String descButton;
	private String pathSelected;
	private String fileExtesion;
	
	public FileExplorerButton(GraphicUserInterface gui, String buttonName, String path, String fileExtesion, int x, int y){
		super(buttonName);
		descButton = buttonName;
		pathSelected = "";
		this.fileExtesion = fileExtesion;
		textArea = null;
		this.pathSelected = path;
		Insets insets = gui.getInsets();
		Dimension size = this.getPreferredSize();
		this.setBounds(x + insets.left, y + insets.top,
	             250, size.height);
		this.gui = gui;
		gui.add(this);
		
		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pathSelected = onClick(descButton, fileExtesion);
				//pathSelected = new String(pathSelected.replace('\\', '/'));
				
				textArea.setText(pathSelected);
				
				//textArea.setSize((int) (pathSelected.length() * 7.22), textArea.getHeight());
			}
		});
		
	}
	
	public String onClick(String descView, String fileExtesion){
		System.out.println("FILECHOOSER STRING - " + fileExtesion);
		String defaultpath = "";
		JFileChooser chooser = ModelSelection.selectPath(descView, fileExtesion);
	    
	    return chooser.getSelectedFile().getPath();
	    
	}

	
	public ScriptParameterTextPane getTextArea() {
		return textArea;
	}

	public void setTextArea(ScriptParameterTextPane textArea) {
		this.textArea = textArea;
	}

	public String getPathSelected() {
		return pathSelected;
	}

	public void setPathSelected(String pathSelected) {
		this.pathSelected = pathSelected;
	}

	public String getPropertyName() {
		return fileExtesion;
	}

	public void setPropertyName(String propertyName) {
		this.fileExtesion = propertyName;
	}
	
	
}

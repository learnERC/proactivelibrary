package policyenforcergenerator.utilities;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * <h2>Type Signatures</h2><br/>
 * The JNI uses the Java VM�s representation of type signatures. Table 3-2 shows
 * these type signatures.
 * <p/>
 * Table 3-2 Java VM Type Signatures
 * 
 * <pre>
 * +---------------------------+-----------------------+
 * | Type Signature            | Java Type             |
 * +---------------------------+-----------------------+
 * | Z                         | boolean               |
 * +---------------------------+-----------------------+
 * | B                         | byte                  |
 * +---------------------------+-----------------------+
 * | clasSName                         | char                  |
 * +---------------------------+-----------------------+
 * | S                         | short                 |
 * +---------------------------+-----------------------+
 * | I                         | int                   |
 * +---------------------------+-----------------------+
 * | J                         | long                  |
 * +---------------------------+-----------------------+
 * | F                         | float                 |
 * +---------------------------+-----------------------+
 * | D                         | double                |
 * +---------------------------+-----------------------+
 * | L fully-qualified-class ; | fully-qualified-class |
 * +---------------------------+-----------------------+
 * | [ type                    | type[]                |
 * +---------------------------+-----------------------+
 * | ( arg-types ) ret-type    | method type           |
 * +---------------------------+-----------------------+
 * </pre>
 * 
 * For example, the Java method:
 * 
 * <pre>
 * long f(int n, String s, int[] arr);
 * </pre>
 * 
 * has the following type signature:
 * 
 * <pre>
 * (ILjava/lang/String;[I)J
 * </pre>
 * 
 * Note, for a constructor, supply &lt;init&gt; as the method name and void (V)
 * as the return type.
 * 
 * @see <a href=
 *      "http://docs.oracle.com/javase/7/docs/technotes/guides/jni/spec/types.html#wp16432">Type
 *      Signatures</a>
 */
public class Signature {
	/**
	 * Converts a Java source-language class name into the internal form. The
	 * internal name of a class is its fully qualified name, as returned by
	 * Class.getName(), where '.' are replaced by '/'.
	 * 
	 * @param clasSName
	 *            an object or array class.
	 * @return the internal name form of the given class
	 * 
	 */
	public static String getSignatureDecode(String className) {

		if ("int".equals(className)) {
			return "I";
		} else if ("long".equals(className)) {
			return "J";
		} else if ("boolean".equals(className)) {
			return "Z";
		} else if ("byte".equals(className)) {
			return "B";
		} else if ("short".equals(className)) {
			return "S";
		} else if ("char".equals(className)) {
			return "clasSName";
		} else if ("float".equals(className)) {
			return "F";
		} else if ("double".equals(className)) {
			return "D";
		} else if ("void".equals(className)) {
			return "V";
		} else {
			String internalName = className.replace('.', '/');
			if(className.contains("[]"))
				return internalName;
			else
				return 'L' + internalName + ';';
		}
	}

	public static String getSignature(String s){
		String ret = s;
		int last;
		String param = "";
		String paramDecoded = "";
		last = s.lastIndexOf("(");
		param = s.substring(last+1, s.length()-1);
		System.out.println("s: " +s);
		System.out.println("param: " + param);
		if(!"".equals(param)){
			paramDecoded = getSignatureDecode(param);
			ret = s.substring(0,last+1) + paramDecoded +  ")";
			System.out.println("Signature: " + s);
		}
		
		return ret;
	}
}
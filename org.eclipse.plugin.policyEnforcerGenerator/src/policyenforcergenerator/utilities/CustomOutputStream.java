package policyenforcergenerator.utilities;

import java.io.IOException;
import java.io.OutputStream;
 
import policyenforcergenerator.gui.textfields.ScriptExecutionScrollPane;
 
public class CustomOutputStream extends OutputStream {
    private ScriptExecutionScrollPane sp;
     
    public CustomOutputStream(ScriptExecutionScrollPane sp) {
        this.sp = sp;
    }
     
    @Override
    public void write(int b) throws IOException {
        // redirects data to the text area
        sp.getTextArea().append(String.valueOf((char)b));
        // scrolls the text area to the end of data
        sp.getTextArea().setCaretPosition(sp.getTextArea().getDocument().getLength());
    }
}
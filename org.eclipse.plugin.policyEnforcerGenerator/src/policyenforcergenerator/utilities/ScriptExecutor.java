package policyenforcergenerator.utilities;

import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ProcessBuilder.Redirect;
import java.util.Map;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import policyenforcergenerator.gui.textfields.ScriptExecutionScrollPane;

public class ScriptExecutor {
	private String path;
	private String xPosedModuleGeneratorName;
	private String xPosedModuleInstallerName;
	private ScriptExecutionScrollPane scrollPane;

	public ScriptExecutor(String path, String xPosedModuleGeneratorName, String xPosedModuleInstallerName, ScriptExecutionScrollPane scrollPane) {
		this.path = path;
		this.xPosedModuleGeneratorName = xPosedModuleGeneratorName;
		this.xPosedModuleInstallerName = xPosedModuleInstallerName;
		this.scrollPane = scrollPane;
	}

	public void runScriptGenerator() throws InterruptedException {
		ScriptGenerator s = new ScriptGenerator(path, xPosedModuleGeneratorName);
		// s.execScript();

		String command = this.path + File.separator + "androidModuleGenerator" + File.separator + this.xPosedModuleGeneratorName;
		try {
			ProcessBuilder pb = new ProcessBuilder(command);
			pb.directory(new File(this.path));
			Map<String, String> env = pb.environment();
			File log = new File(this.path + File.separator + "androidModuleGenerator" + File.separator + "logModuleGenerator.txt");
			if (log.createNewFile()) {
				System.out.println("File created: " + log.getName());
			} else {
				System.out.println("File already exists.");
			}
			FileWriter myWriter = new FileWriter(this.path + File.separator + "androidModuleGenerator" + File.separator + "logModuleGenerator.txt");
			myWriter.write("");
			myWriter.close();

			System.out.println(log.getAbsolutePath());
			pb.redirectErrorStream(true);
			pb.redirectOutput(Redirect.appendTo(log));
			Process p = pb.start();
			assert pb.redirectInput() == Redirect.PIPE;
			assert pb.redirectOutput().file() == log;
			assert p.getInputStream().read() == -1;
			
			ImageIcon icon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/loading.gif"));
			
			final JLabel label = new JLabel(icon);
			label.setText("Proactive Module is being generated...");
			label.setHorizontalTextPosition(JLabel.CENTER);
			label.setVerticalTextPosition(JLabel.TOP);
			
			
			new Timer(0, new ActionListener() {

			      @Override
			      public void actionPerformed(ActionEvent e) {
			        if (!p.isAlive()) {
			          ((Timer) e.getSource()).stop();
			          Window win = SwingUtilities.getWindowAncestor(label);
			          if(win != null) win.setVisible(false);
			        }
			      }
			    }).start();
			
			while(p.isAlive()){
				JOptionPane.showOptionDialog(null, label, "Generation", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
			}
			
			JOptionPane.showMessageDialog(new JFrame(), "Generation concluded. Log available at " + this.path + File.separator + "androidModuleGenerator" + File.separator + "logModuleGenerator.txt");
			
			scrollPane.getTextArea().setText("");
			scrollPane.getGui().setMaxFrameHeight();
			scrollPane.getGui().setLocationRelativeTo(null);
			Scanner myReader = new Scanner(log);
			while (myReader.hasNextLine()) {
				String data = myReader.nextLine();
				scrollPane.getTextArea().append(data + "\n");
			}
			myReader.close();
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void runScriptInstaller() throws InterruptedException {
		ScriptGenerator s = new ScriptGenerator(path, xPosedModuleInstallerName);
		// s.execScript();

		String command = this.path + File.separator + "androidModuleGenerator" + File.separator + this.xPosedModuleInstallerName;
		try {
			ProcessBuilder pb = new ProcessBuilder(command);
			pb.directory(new File(this.path));
			Map<String, String> env = pb.environment();
			File log = new File(this.path + File.separator + "androidModuleGenerator" + File.separator + "logModuleInstaller.txt");
			if (log.createNewFile()) {
				System.out.println("File created: " + log.getName());
			} else {
				System.out.println("File already exists.");
			}
			FileWriter myWriter = new FileWriter(this.path + File.separator +  "androidModuleGenerator" + File.separator + "logModuleInstaller.txt");
			myWriter.write("");
			myWriter.close();

			System.out.println(log.getAbsolutePath());
			pb.redirectErrorStream(true);
			pb.redirectOutput(Redirect.appendTo(log));
			
			Process p = pb.start();
			assert pb.redirectInput() == Redirect.PIPE;
			assert pb.redirectOutput().file() == log;
			assert p.getInputStream().read() == -1;
			
			ImageIcon icon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/loading.gif"));
			
			final JLabel label = new JLabel(icon);
			label.setText("Proactive Module is being installed...");
			label.setHorizontalTextPosition(JLabel.CENTER);
			label.setVerticalTextPosition(JLabel.TOP);

			new Timer(0, new ActionListener() {

			      @Override
			      public void actionPerformed(ActionEvent e) {
			        if (!p.isAlive()) {
			          ((Timer) e.getSource()).stop();
			          Window win = SwingUtilities.getWindowAncestor(label);
			          win.setVisible(false);
			        }
			      }
			    }).start();
			
			while(p.isAlive()){
				JOptionPane.showOptionDialog(null, label, "Installation", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, new Object[]{}, null);
			}
			
			JOptionPane.showMessageDialog(new JFrame(), "Installation concluded. Log available at " + this.path + File.separator + "androidModuleGenerator" + File.separator + "logModuleInstaller.txt");
			
			scrollPane.getTextArea().setText("");
			Scanner myReader = new Scanner(log);
			while (myReader.hasNextLine()) {
				String data = myReader.nextLine();
				scrollPane.getTextArea().append(data + "\n");
			}
			myReader.close();
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public ScriptExecutionScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(ScriptExecutionScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

}

package policyenforcergenerator.utilities;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.resources.ResourcesPlugin;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ModelSelection {
	
	public String selectModel(){
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Scegli il tuo modello");
	    FileNameExtensionFilter filter = new FileNameExtensionFilter(
	        "EDITAUTOMATON", "editautomaton");
	    chooser.setFileFilter(filter);
	    int returnVal = chooser.showOpenDialog(null);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	       System.out.println("ModelSelection.selectModel() -- You chose to open this file: " +
	            chooser.getSelectedFile().getName());
	    }
	    
		return chooser.getSelectedFile().getPath();
		
	}
	
	public JFileChooser selectGenerator(){
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Seleziona il percorso del generatore del modulo Xposed");
	    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
//	    int returnVal = chooser.showOpenDialog(null);
//	    if(returnVal == JFileChooser.APPROVE_OPTION) {
//	       System.out.println("ModelSelection.selectGenerator() -- You chose to open this path: " +
//	            chooser.getSelectedFile().getName());
//	    }
//	    
//		return chooser.getSelectedFile().getPath();
		return chooser;
	}
	
	public static JFileChooser selectPath(String descView, String fileExtesion){
		System.out.println("FILECHOOSER STRING - " + fileExtesion);
		String defaultpath = "";
		JFileChooser chooser = null;

		if(fileExtesion != null && !"".equals(fileExtesion)){
			defaultpath = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString();
			chooser = new JFileChooser(defaultpath);
			chooser.setDialogTitle(descView);
		    FileNameExtensionFilter filter = new FileNameExtensionFilter(
		        fileExtesion, fileExtesion.toLowerCase());
		    chooser.setFileFilter(filter);
		}else{
			defaultpath = ResourcesPlugin.getWorkspace().getRoot().getLocation().removeLastSegments(1).toString();
			chooser = new JFileChooser(defaultpath);
			System.out.println("LAST SEGMENT - " + defaultpath);
			chooser.setDialogTitle(descView);
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		}
		
	    int returnVal = chooser.showOpenDialog(null);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	       System.out.println("ModelSelection.selectModel() -- You chose to open this file: " +
	            chooser.getSelectedFile().getName());
	    }
	    
		return chooser;
	}
	
	public static String extractModelName(String path){
		String editAutomatonName = null;
		
		try{
		    File inputFile = new File(path);
	        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	        Document doc = dBuilder.parse(inputFile);
	        doc.getDocumentElement().normalize();
	        
	        NodeList nList = doc.getElementsByTagName("editautomaton:EditAutomaton");
	        Node nNode = nList.item(0);
	        //System.out.println("\nCurrent Element :" + nNode.getNodeName());
	        
	        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	        	editAutomatonName = ((Element) nNode).getAttribute("name");
	        	System.out.println("ModelSelection.extractModelName() -- The model name is  : " 
	                  + editAutomatonName);
	        }
	        
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		
			extractEventList(path);
		return editAutomatonName.replace(" ", "");
		
	}
	
	/**
	 * metodo di supporto per la creazione degli event_list necessari per la mappatura del modelllo all'interno del Test4Enforcer
	 * 
	 * @param path
	 */
	public static void extractEventList(String path){	
		/*Creazione Event List da utilizzare per Test4Enforcer*/
		
		String interceptedAction = "";
		String actionToPerform = "";
		String tmp3 = "";
		String path2 = path;
		String sep = File.separator;
		path2 = path2.substring(0, path.lastIndexOf(sep));
		File log = new File(path2 + File.separator + "eventList.txt");
		try {
			if (log.createNewFile()) {
				System.out.println("File created: " + log.getName());
			} else {
				System.out.println("File already exists.");
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ArrayList <String> s = new ArrayList<String>(); //contiene event_list
		ArrayList <String> s2 = new ArrayList<String>(); //contiene event_list_of_list_input
		String[] actions;
		String eventList = "";
		
		System.out.println("-------------------------------------------\n\n");
		System.out.println("Creazione Event List da utilizzare per Test4Enforcer\n");
		System.out.println("-------------------------------------------\n\n");
		
		try{
		    File inputFile = new File(path);
		    System.out.println("PATH - " + path2);
	        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	        Document doc = dBuilder.parse(inputFile);
	        doc.getDocumentElement().normalize();
	        
	        NodeList nList = doc.getElementsByTagName("transitions");
	        Node nNode = null;
	        Class c;
	        int last;
	        
	        for (int i = 0; i < nList.getLength(); i++) {
	        	nNode = nList.item(i);
	        	interceptedAction = ((Element) nNode).getAttribute("interceptedAction");
	        	actionToPerform = ((Element) nNode).getAttribute("actionToPerform");
	        	
	        	//rimozione after# e before# dalle stringhe
	        	interceptedAction = interceptedAction.replace("after#", "");
	        	actionToPerform = actionToPerform.replace("after#", "");
	        	interceptedAction = interceptedAction.replace("before#", "");
	        	actionToPerform = actionToPerform.replace("before#", "");
	        	
	        	//rimozione parentesi tonde in eccesso
	        	last = interceptedAction.lastIndexOf("(");
	        	interceptedAction = interceptedAction.substring(0, last).replace("()","") + interceptedAction.substring(last);
	        	
	        	
	        	
	        	s.add(new String("\"" + Signature.getSignature(interceptedAction) + "\""));
	        		
	        	System.out.println("interceptedAction - " + interceptedAction);
	        	System.out.println("actionToPerform - " + actionToPerform);
	        	actions = actionToPerform.split(";");
	        	System.out.println("actions: " + actions);
	        	System.out.println("actions.length: " + actions.length);
	        	actionToPerform = "";
	        	
	        	for (int j = 0; j < actions.length; j++) {
	        		last = actions[j].lastIndexOf("(");
	        		actions[j] = actions[j].substring(0, last).replace("()","") + actions[j].substring(last);
				}
	        	
	        	if(actions.length == 1){
	        		actions[0] = Signature.getSignature(actions[0]);
	        		actionToPerform = actions[0];
	        		actionToPerform = "(\"" + actionToPerform + "\", False)";
	        	}else{
		        	for (int j = 0; j < actions.length; j++) {
		        		actions[j] = Signature.getSignature(actions[j]);
		        		tmp3 = actions[j];
		        		if(j == actions.length-1){
		        			tmp3 = "(\"" + tmp3 + "\", True)";
		        		}else{
		        			tmp3 = "(\"" + tmp3 + "\", False)";
		        		}
		        		if(j != actions.length-1)
		        			tmp3 += ", ";
		        		
		        		actionToPerform += tmp3;
		        			
					}
		        }
	        	s2.add(new String("[" + actionToPerform + "]"));
	        }
	        
	        System.out.println("\nactionToPermorm: " + s2);
	        
	        Set<String> set = new LinkedHashSet<>();
	        
	        set.addAll(s);
	        s.clear();
	        s.addAll(set);
	        
	        System.out.println("\n\ninterceptedAction: " + s);
	        eventList += "event_list = " + s + "\n";
	        
	        System.out.println("\n\nactionToPermorm: " + s2);
	        eventList += "event_list_of_list_input = " + s2;
	        
	        System.out.println("\n\n\n-----------\n" + eventList + "\n\n\n");
	        
	        
	        FileWriter myWriter = new FileWriter(path2 + File.separator + "eventList.txt");
	        myWriter.write("");
			myWriter.write(eventList);
			myWriter.close();
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		
		
	}
}

package policyenforcergenerator.utilities;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class ScriptGenerator {
	private String path;
	private String projectGeneratorScript;
	private String projectInstallerScript;
	private String projectGeneratorScriptName;
	private String projectInstallerScriptName;


	public String getProjectGeneratorScript() {
		return projectGeneratorScript;
	}

	public void setProjectGeneratorScript(String projectGeneratorScript) {
		this.projectGeneratorScript = projectGeneratorScript;
	}

	public String getProjectInstallerScript() {
		return projectInstallerScript;
	}

	public void setProjectInstallerScript(String projectInstallerScript) {
		this.projectInstallerScript = projectInstallerScript;
	}

	public String getProjectGeneratorScriptName() {
		return projectGeneratorScriptName;
	}

	public void setProjectGeneratorScriptName(String projectGeneratorScriptName) {
		this.projectGeneratorScriptName = projectGeneratorScriptName;
	}

	public String getProjectInstallerScriptName() {
		return projectInstallerScriptName;
	}

	public void setProjectInstallerScriptName(String projectInstallerScriptName) {
		this.projectInstallerScriptName = projectInstallerScriptName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public ScriptGenerator(String pathEditAutomaton, String editAutomatonName, String pathGenerator, String version, String description) throws StringIndexOutOfBoundsException {
		this.path = pathGenerator;
		editAutomatonName = editAutomatonName.replace(" ", "");
		System.out.println("PATH GENERATOR - " + pathGenerator);
		
		this.projectGeneratorScriptName = "generatorXPosedModule.bat";
		this.projectInstallerScriptName = "XPosedModuleInstaller.bat";
		init(pathEditAutomaton, editAutomatonName, version, description);
		createScript();
	}

	public ScriptGenerator(String path, String scriptName) {
		this.path = path;
		this.projectGeneratorScriptName = scriptName;
	}

	public void init(String pathEditAutomaton, String editAutomatonName, String version, String description) {
		//editAutomatonName = editAutomatonName.substring(0, 1).toUpperCase() + editAutomatonName.substring(1);
		description = description.replace("\n", "\\n");
		
		if(pathEditAutomaton.contains(" ")) pathEditAutomaton = "\"" + pathEditAutomaton + "\"";
			
		if (System.getProperty("os.name").startsWith("Windows")) {
		projectGeneratorScript = String.join("\n", Arrays.asList(
			"",
			"cd \"" + this.path + "\"",
			"",
			"if not exist " + "\"" + this.path + File.separator + "editautomaton" + File.separator + "\" (",
			"	mkdir " + "\"" + this.path + File.separator + "editautomaton\"",
			")",
			"copy " + pathEditAutomaton + " \"" + this.path + File.separator + "editautomaton" + File.separator +  "default.editautomaton\"",
			"",
			"call mvn clean compile",
			"cd modenforcer",
			"",
			"call mvn archetype:generate -B -Dversion=" + version + " -Ddescription=\"" + description + "\" -DarchetypeGroupId=local.progetto -DarchetypeArtifactId=modenforcer -DarchetypeVersion=1.0 -DgroupId=com.proactive." + editAutomatonName.toLowerCase() + " -DartifactId=" + editAutomatonName + " -DLowerArtifactId=" + editAutomatonName.toLowerCase() + " -DsourceDirectory=\"" + this.path + File.separator + "modenforcer" + File.separator + "src-gen\" -DarchetypeCatalog=local",
			"copy \"" + this.path + File.separator + "modenforcer" + File.separator + "src-gen" + File.separator + editAutomatonName + ".java\" \"" + this.path + File.separator + "modenforcer" + File.separator + editAutomatonName + File.separator + "app" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "com" + File.separator + "proactive" + File.separator + editAutomatonName.toLowerCase() + File.separator + editAutomatonName + ".java\"",
			"",
			"cd " + editAutomatonName,
			"call mvn validate"
		));

		projectInstallerScript = String.join("\n", Arrays.asList(
			"@echo off",
			"",
			"cd \"" + this.path + File.separator + "modenforcer" + File.separator + editAutomatonName + "\"",
			"",
			"call gradle build",
			"call gradle installDebug",
			"adb shell \"[ -f /data/app/com.proactive." + editAutomatonName.toLowerCase() + "-1/base.apk ] && echo /data/app/com.proactive." + editAutomatonName.toLowerCase() + "-1/base.apk >> /data/data/de.robv.android.xposed.installer/conf/modules.list\"",
			"adb shell \"[ -f /data/app/com.proactive." + editAutomatonName.toLowerCase() + "-1/base.apk ] && reboot\""
		));
	} else {
		projectGeneratorScript = String.join("\n", Arrays.asList(
				"#!/bin/bash",
				"cd \"" + this.path + "\"",
				"",
				"if ! [ -d \"" + this.path + File.separator + "editautomaton\" ]; then",
				"	mkdir \"" + this.path + File.separator + "editautomaton\"",
				"fi",
				"cp " + pathEditAutomaton + " \"" + this.path + File.separator + "editautomaton" + File.separator +  "default.editautomaton\"",
				"",
				"mvn clean compile",
				"cd modenforcer",
				"",
				"mvn archetype:generate -B -Dversion=" + version + " -DarchetypeGroupId=local.progetto -DarchetypeArtifactId=modenforcer -DarchetypeVersion=1.0 -DgroupId=com.proactive." + editAutomatonName.toLowerCase() + " -DartifactId=" + editAutomatonName + " -DLowerArtifactId=" + editAutomatonName.toLowerCase() + " -DsourceDirectory=\"" + this.path + File.separator + "modenforcer" + File.separator + "src-gen\" -DarchetypeCatalog=local",
				"cp \"" + this.path + File.separator + "modenforcer" + File.separator + "src-gen" + File.separator + editAutomatonName + ".java\" \"" + this.path + File.separator + "modenforcer" + File.separator + editAutomatonName + File.separator + "app" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "com" + File.separator + "proactive" + File.separator + editAutomatonName.toLowerCase() + File.separator + editAutomatonName + ".java\"",
				"",
				"cd " + editAutomatonName,
				"mvn validate"
			));

			projectInstallerScript = String.join("\n", Arrays.asList(
				"#!/bin/bash",
				"",
				"cd \"" + this.path + File.separator + "modenforcer" + File.separator + editAutomatonName + "\"",
				"",
				"gradle build",
				"gradle installDebug"
			));
		
	}

		System.out.println("editAutomatonName - " + editAutomatonName);
	}

	public void createScript() {
		System.out.println("\n\nSONO DENTRO createScript()");
		// String pathJava = new String(this.path.replace('\\', '/'));

		try {

			File theDir = new File(path + File.separator + "androidModuleGenerator");
			if (!theDir.exists()){
			    theDir.mkdirs();
			}

			//file con instruzione di generazione modulo XPosed
			File myObj = new File(theDir + File.separator + this.projectGeneratorScriptName);
			System.out.println(myObj.getAbsolutePath());
			if (myObj.createNewFile()) {
				System.out.println("File created: " + myObj.getName());
			} else {
				System.out.println("File already exists.");
			}
			myObj.setExecutable(true);

			FileWriter myWriter = new FileWriter(theDir + File.separator + this.projectGeneratorScriptName);
			myWriter.write(projectGeneratorScript);
			myWriter.close();
			System.out.println("Successfully wrote to the file. projectGeneratorScript");

			//file con istruzionei di build gradle
			myObj = new File(theDir + File.separator + this.projectInstallerScriptName);
			System.out.println(myObj.getAbsolutePath());
			if (myObj.createNewFile()) {
				System.out.println("File created: " + myObj.getName());
			} else {
				System.out.println("File already exists.");
			}
			myObj.setExecutable(true);

			myWriter = new FileWriter(theDir + File.separator + this.projectInstallerScriptName);
			myWriter.write(projectInstallerScript);
			myWriter.close();
			System.out.println("Successfully wrote to the file. projectGeneratorScript");


		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

	public void execScriptGenerator() {
		System.out.println("\n\nSONO DENTRO loadScript()");
		try {
			String path = "cmd /c start " + this.path + File.separator + this.projectGeneratorScriptName;
			Runtime rn = Runtime.getRuntime();
			Process pr = rn.exec(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
